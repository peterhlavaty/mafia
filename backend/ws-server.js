const WebSocket = require('ws');
const debug = require('debug')('backend:server');
const messageTypes = require('./massage-handlers/message-types');
const { gameTypes, unfinishedGames, playerGameMap } = require('./games');

const wss = new WebSocket.Server({ noServer: true });
const wsMap = new Map();

wss.on('connection', (ws, req, client) => {
  debug("client connected: ", client);
  wsMap.set(client.id, ws);

  ws.send(JSON.stringify({
    type: 'unfinishedGames',
    data: unfinishedGames
  }));

  ws.on('message', (message) => {
    debug(`received: ${message} from ${client.email}`);
    const parsedMessage = JSON.parse(message);
    if (messageTypes[parsedMessage.type]) {
      // call dedicated message handler
      messageTypes[parsedMessage.type](parsedMessage, client, wsMap);
    } else {
      debug('WARNING: unknown message');
    }
    // ws.send(JSON.stringify({ data: `${client} sent -> ${message}`}));
    // wss.clients.forEach(c => {
    //   if (c !== ws && c.readyState === WebSocket.OPEN) {
    //     c.send(JSON.stringify({ data: `${client} sent -> ${message}`}));
    //   }
    // });
  });

  ws.on('close', () => {
    wsMap.delete(client.id);
    debug(`connection closed with ${client.email}`);
  });
});

module.exports = {
    wss,
    wsMap
};