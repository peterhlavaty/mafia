const gameTypes = {
  duel: {
    name: 'duel',
    numberOfPlayers: 2,
  },
  classic: {
    name: 'classic',
    numberOfPlayers: 4,
  },
};

const unfinishedGames = new Map();
const playerGameMap = new Map();
for (let i = 2;i <= 10;i++) {
  unfinishedGames.set(i,[]);
}

module.exports = {
  gameTypes,
  unfinishedGames,
  playerGameMap,
};
