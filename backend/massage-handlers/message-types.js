const startGameHandler = require('./start-game.handler');
const joinGameHandler = require('./join-game.handler');
const publicVoteHandler = require('./publicVoteHandler');
const privateVoteHandler = require('./privateVoteHandler');
const voteStart = require('./voteStart');

const messageTypes = {
    'startGame': startGameHandler,
    'joinGame': joinGameHandler,
    'publicVote': publicVoteHandler,
    'privateVote': privateVoteHandler,
    'voteStart': voteStart,
};

module.exports = messageTypes;
