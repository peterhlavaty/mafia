const debug = require('debug')('backend:server');
const { gameTypes, unfinishedGames, playerGameMap } = require('../games');
const chooseRoles = require('../game-logic/gameLogic');

function startGameHandler({ data }, client, wsMap) {
  const games = unfinishedGames.get(+data.playerCount);
  const gameType = +data.playerCount;
  if (games) {
    const game = {
      type: gameType,
      players: [client],
      started: false,
    }
    playerGameMap.set(+client.id, game);
    games.push(game);
    wsMap.forEach((value, key)=>{
      if(key!==client.id){
        if (value) {
          value.send(JSON.stringify({
            type: 'joinableGame',
            data: {
              playerId: +client.id,
              playerName: client.nickname,
              playerEmail: client.email,
            }
          }));
        } else {
          debug('WARNING: player disconnected');
        }
      }
    })
  } else {
    debug('ERROR: Game type not found');
  }
};

module.exports = startGameHandler;