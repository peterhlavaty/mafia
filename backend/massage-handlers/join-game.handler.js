const debug = require('debug')('backend:server');
const { gameTypes, unfinishedGames, playerGameMap } = require('../games');
const chooseRoles = require('../game-logic/gameLogic');

function joinGameHandler({ data }, client, wsMap) {
    // const games = unfinishedGames[+data.playerCount];
    // const gameType = +data.playerCount;
    // if (games) {
    //     const gameToJoin = games.find(g => !g.started);
    let gameToJoin = playerGameMap.get(+data.playerId);
        // if (gameToJoin) {
    gameToJoin.players.push(client);
    if (gameToJoin.players.length === +gameToJoin.type) {
        gameToJoin.started = true;
        // const positions = {};
        // gameToJoin.players.forEach(player => {
        //     positions[player.id] = {
        //         x: getRandomInt(50, 750),
        //         y: getRandomInt(50, 550),
        //     }
        // });
        let roles = chooseRoles(gameToJoin.players.length);
        gameToJoin.players.forEach(player => {
            playerGameMap.set(+player.id, gameToJoin);
            let index = Math.floor(Math.random() * (gameToJoin.players.length));
            let role = roles[index];
            player.role = role;
            roles.splice(index, 1);
        })

        let allRoles = [];
        gameToJoin.players.forEach(player => {
            allRoles.push(player.nickname + ' - ' + player.role);
        })

        gameToJoin.players.forEach(player => {
            const playerWs = wsMap.get(player.id);
            if (playerWs) {
                playerWs.send(JSON.stringify({
                    type: 'startGame',
                    data: {
                        myId: player.id,
                        role : player.role,
                        allRoles,
                    }
                }));
            } else {
                debug('WARNING: player disconnected');
            }
        })
    }
        // } else {
        //     games.push({
        //         type: gameType,
        //         players: [client],
        //         started: false,
        //     });
        // }
    // } else {
    //     debug('ERROR: Game type not found');
    // }
};

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

module.exports = joinGameHandler;