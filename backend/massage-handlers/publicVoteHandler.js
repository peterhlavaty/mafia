const { gameTypes, unfinishedGames, playerGameMap } = require('../games');

function publicVoteHandler({ data }, client, wsMap){
    let game = playerGameMap.get(+client.id);
    game.votes.push(client.nickname + ' ' + data.vote);
    if(game.players.length===game.votes.length){
        game.players.forEach(player => {
            const playerWs = wsMap.get(player.id);
            if (playerWs) {
                playerWs.send(JSON.stringify({
                    type: 'votes',
                    data: {
                        votes: game.votes,
                    }
                }));
            } else {
                debug('WARNING: player disconnected');
            }
        })
    }
}

module.exports = publicVoteHandler;