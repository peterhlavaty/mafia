const { gameTypes, unfinishedGames, playerGameMap } = require('../games');

function privateVoteHandler({ data }, client, wsMap){
    console.log(playerGameMap);
    let game = playerGameMap.get(+client.id);
    game.votes.push(data.vote);
    if(game.voteCount===game.votes.length){
        game.players.forEach(player => {
            const playerWs = wsMap.get(player.id);
            if (playerWs) {
                playerWs.send(JSON.stringify({
                    type: 'votes',
                    data: {
                        votes: game.votes,
                    }
                }));
            } else {
                debug('WARNING: player disconnected');
            }
        })
    }
}

module.exports = privateVoteHandler;