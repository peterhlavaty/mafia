const { gameTypes, unfinishedGames, playerGameMap } = require('../games');

function voteStart({ data }, client, wsMap){
    let game = playerGameMap.get(+client.id);
    game.votes = [];
    game.voteCount = data.voteCount? data.voteCount: game.players.length;
    game.players.forEach(player => {
        const playerWs = wsMap.get(player.id);
        if (playerWs) {
            playerWs.send(JSON.stringify({
                type: 'voteStart',
                data: {
                    votes: game.votes,
                    voteCount: game.voteCount,
                }
            }));
        } else {
            debug('WARNING: player disconnected');
        }
    })
}

module.exports = voteStart;