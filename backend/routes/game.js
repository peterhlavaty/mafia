const express = require('express');
const router = express.Router();
const db = require('../db');
const games = require('../games');

const gameTypes = {
  duel: {
    name: 'duel',
    numberOfPlayers: 2,
  },
  classic: {
    name: 'classic',
    numberOfPlayers: 5,
  },
};

router.get('/join', async (req, res, next) => {
  try {
    const gameType = gameTypes[req.query.type];
    if (gameType) {
      const gameToJoin = games.find(g => g.type === gameType.name);
      if (gameToJoin) {
        gameToJoin.players.push(req.user.id);
        if (gameToJoin.players.length === gameType.numberOfPlayers) {

        }
      } else {
        games.push({
          type: gameType.name,
          players: [req.user.id],
        });
        res.json({
          gameId: 1
        })
      }
    } else {
      throw 'BAD_REQUEST';
    }
  } catch (error) {
    return next(error);
  }
});

module.exports = router;
