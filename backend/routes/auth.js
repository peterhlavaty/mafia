const express = require('express');
const passport = require('passport');
const router = express.Router();
const getHashedPassword = require('../auth/get-hashed-password');
const db = require('../db');

router.post('/registration', async (req, res, next) => {
  try {
    console.log('registration');
    const user = await db('user').where({ email: req.body.email });
    console.log(user);
    if (user.length !== 0) {
      res.status(409).send('Email already registered');
    } else {
      const newUser = (await db('user').returning(['nickname', 'email', 'password']).insert({
        ...req.body,
        password: getHashedPassword(req.body.password),
      }))[0];
      console.log(newUser);
      req.login(newUser, (err) => {
        if (err) throw err;
        res.status(201).end();
      });
    }
  } catch (err) {
    next(err);
  }
  
});

router.post('/login', passport.authenticate('local'), (req, res) => {
  res.status(200).end();
});

router.get('/checkLogin', (req, res) => {
  res.send(!!req.user);
});

router.get('/logout', (req, res) => {
  req.logout();
  res.status(200).end();
});

module.exports = router;
