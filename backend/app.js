const express = require('express');
const session = require('express-session');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const debug = require('debug')('backend:server');
const http = require('http');
const cors = require('cors');
const passport = require('passport');
const localStrategy = require('./auth/local-strategy');
const db = require('./db');
const { wss } = require('./ws-server');

require('dotenv').config();

const indexRouter = require('./routes/index');
const authRouter = require('./routes/auth');

const app = express();

const corsOptions = {
  origin: process.env.CLIENT_ADDRESS
};
const sessionParser = session({
  secret: "cats",
  resave: false,
  saveUninitialized: false
});

app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sessionParser);
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

passport.use('local', localStrategy);
passport.serializeUser((user, done) => {
  done(null, user.email);
});
passport.deserializeUser(async (email, done) => {
  try {
    const user = (await db('user').where({ email }))[0];
    if (!user) {
      return done(new Error('user not found'));
    }
    done(null, user);
  } catch (e) {
    done(e);
  }
});

app.use('/api', indexRouter);
app.use('/api/auth', authRouter);

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

var server = http.createServer(app);

server.on('upgrade', function upgrade(request, socket, head) {
  sessionParser(request, {}, async function(err) {
    try {
      if (err) throw err;
      debug('Connection received with sessionId ' + request.session.id);
      debug('User: ', request.session.passport.user);
      const user = (await db('user').where({ email: request.session.passport.user }))[0];
      if (!user) {
        return console.error('User not found. WS connection not established.');
      }
      wss.handleUpgrade(request, socket, head, function done(ws) {
        wss.emit('connection', ws, request, user);
      });
    } catch (e) {
      console.error('Error occured. WS connection not established.', e);
    }
  });
});

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

module.exports = app;
