import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './game/game.component';
import { UserConsoleComponent } from './user-console/user-console.component';
import { NewGameComponent } from './new-game/new-game.component';
import { StatsComponent } from './stats/stats.component';
import { LeaderboardsComponent } from './leaderboards/leaderboards.component';
import { FinishedGameComponent } from './finished-game/finished-game.component';
import { LoginRegisterWrapperComponent } from './login-register-wrapper/login-register-wrapper.component';
import { AuthGuard } from './auth/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginRegisterWrapperComponent },
  { path: 'app', component: UserConsoleComponent, canActivate: [AuthGuard], children: [
    { path: 'game', component: GameComponent, canActivate: [AuthGuard] },
    { path: 'new_game', component: NewGameComponent },
    { path: 'stats', component: StatsComponent },
    { path: 'leaderboards', component: LeaderboardsComponent },
    { path: 'game/:id', component: FinishedGameComponent },
    { path: '', redirectTo: '/app/new_game', pathMatch: 'full' },
  ]},
  { path: '', redirectTo: '/app/new_game', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
