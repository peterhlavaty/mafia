import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { WsService } from '../ws.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private wsService: WsService,
  ) { }

  register(data): Observable<void> {
    return this.http.post<void>('/api/auth/registration', data);
  }

  login(data): Observable<void> {
    return this.http.post<void>('/api/auth/login', data);
  }

  logout(): Observable<void> {
    return this.http.get<void>('/api/auth/logout');
  }

  checkLogin(): Observable<boolean> {
    return this.http.get<boolean>('/api/auth/checkLogin').pipe(
      tap(
        (isLoggedIn) => {
          if (isLoggedIn && !this.wsService.isConnected) {
            this.wsService.createWsConnection();
          }
        }
      )
    );
  }

  test() {
    return this.http.get('/api');
  }
}
