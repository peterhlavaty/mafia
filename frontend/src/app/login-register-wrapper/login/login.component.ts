import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { WsService } from 'src/app/ws.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  public invalidCredentials = false;

  public loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private wsService: WsService,
  ) { }

  ngOnInit(): void {
    this.loginForm.valueChanges.subscribe(() => this.invalidCredentials = false);
  }

  onSubmit() {
    this.loginForm.markAllAsTouched();

    if (!this.loginForm.invalid) {
      this.authService.login(this.loginForm.value).subscribe(
        () => {
          this.wsService.createWsConnection();
          this.router.navigate(['/app/new_game']);
        },
        err => {
          console.log('asdf');
          this.invalidCredentials = true;
        },
      );
    }
  }

}
