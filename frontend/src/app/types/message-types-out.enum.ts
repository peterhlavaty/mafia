export enum MessageTypesOut {
  START_GAME = 'startGame',
  JOIN_GAME = 'joinGame',
  VOTE_START = 'voteStart',
  PUBLIC_VOTE = 'publicVote',
  PRIVATE_VOTE = 'privateVote',
  SEND_POSITION = '',
}
