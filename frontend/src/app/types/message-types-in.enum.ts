export enum MessageTypesIn {
  START_GAME = 'startGame',
  JOINABLE_GAME = 'joinableGame',
  UNFINISHED_GAMES = 'unfinishedGames',
  VOTE_START = 'voteStart',
  VOTES = 'votes',
}
