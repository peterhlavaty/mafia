import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { WsService } from '../ws.service';

@Component({
  selector: 'app-user-console',
  templateUrl: './user-console.component.html',
  styleUrls: ['./user-console.component.less']
})
export class UserConsoleComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private wsService: WsService,
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout().subscribe(
      () => {
        this.wsService.closeConnection();
        this.router.navigate(['/login']);
      },
      err => console.log(err),
    );
  }

  test() {
    this.authService.test().subscribe(
      () => console.log('asdf'),
      err => console.log(err),
    );
  }

  // send() {
  //   this.wsService.sendMessage({
  //     type: 'joinGame',
  //     data: {
  //       message: 'asdf',
  //     }
  //   });
  // }

}
