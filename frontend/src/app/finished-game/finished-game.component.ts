import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-finished-game',
  templateUrl: './finished-game.component.html',
  styleUrls: ['./finished-game.component.less']
})
export class FinishedGameComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
