import {Injectable} from '@angular/core';
import {webSocket, WebSocketSubject} from "rxjs/webSocket";
import {MessageTypesOut} from './types/message-types-out.enum';
import {tap} from 'rxjs/operators';
import {MessageTypesIn} from './types/message-types-in.enum';
import {Router} from '@angular/router';
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class WsService {
  private socket: WebSocketSubject<{ type: MessageTypesIn | MessageTypesOut, data: object }> = webSocket(environment.wsUrl);

  get isConnected() {
    return this.socket && !this.socket.closed;
  }

  constructor(private router: Router) { }

  public get socketSubject() {
    return this.socket.pipe(
      tap({
        next: msg => console.log('message received: ', msg), // Called whenever there is a message from the server.
        error: err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
        complete: () => console.log('complete') // Called when connection is closed (for whatever reason).
      })
    );
  }

  public createWsConnection() {
    // this.socket.subscribe();
    this.socket.subscribe(
      msg => {
        console.log('message received: ' + msg); // Called whenever there is a message from the server.
        if (msg.type === MessageTypesIn.START_GAME) {
          this.router.navigateByUrl('/app/game', {state: msg.data});
        }
      },
      err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      () => console.log('complete') // Called when connection is closed (for whatever reason).
    );
  }

  public sendMessage(message: { type: MessageTypesOut, data: object }) {
    this.socket.next(message);
  }

  public closeConnection() {
    this.socket.complete();
  }
}
