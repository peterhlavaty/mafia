import * as Phaser from 'phaser';
import { Injectable } from '@angular/core';
import { WsService } from 'src/app/ws.service';

@Injectable({
  providedIn: 'root'
})
export class MainScene extends Phaser.Scene {
  player: Phaser.GameObjects.Rectangle;
  positions: { [key: string]: {x: number, y: number} };
  myId: number;
  otherPlayers: {
    [key: string]: Phaser.GameObjects.Rectangle
  } = {};

  constructor(private wsService: WsService) {
    super({ key: 'main' });
  }

  init(data) {
    console.log('initial data', data);
    this.positions = data.positions;
    this.myId = data.myId;
  }

  preload() {
    console.log('preload method');
  }

  create() {
    console.log('create method');
    this.cameras.main.backgroundColor.setTo(240,240,240);

    Object.entries(this.positions).forEach(([id, {x, y}]) => {
      if (+id === this.myId) {
        this.player = this.add.rectangle(x, y, 20, 20, 0xff0000);
        this.physics.add.existing(this.player);
      } else {
        this.otherPlayers[id] = this.add.rectangle(x, y, 20, 20, 0xff0000);
        this.physics.add.existing(this.otherPlayers[id]);
      }
    });
  }

  update() {
    console.log('update method');
    const cursors = this.input.keyboard.createCursorKeys();
    const playerBody = <Phaser.Physics.Arcade.Body>this.player.body;
    playerBody.setCollideWorldBounds(true);
    if (cursors.left.isDown) {
      playerBody.setVelocityX(-160);
    } else if (cursors.right.isDown) {
      playerBody.setVelocityX(160);
    } else {
      playerBody.setVelocityX(0);
    }

    if (cursors.up.isDown) {
      playerBody.setVelocityY(-160);
    } else if (cursors.down.isDown) {
      playerBody.setVelocityY(160);
    } else {
      playerBody.setVelocityY(0);
    }


    // this.wsService.sendMessage({
    //   type:
    // })
  }
}
