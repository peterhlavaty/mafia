import { Component, OnInit, OnChanges, NgZone } from '@angular/core';
import * as Phaser from 'phaser';
import { MainScene } from './main-scene/main.scene';
import { WsService } from '../ws.service';
import { filter } from 'rxjs/operators';
import { MessageTypesIn } from '../types/message-types-in.enum';
import {MessageTypesOut} from "../types/message-types-out.enum";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less']
})
export class GameComponent implements OnInit {

  // gameStarted = false;
  role:string;
  voteInProgress = false;
  voted = false;
  voteResult: any;
  allRoles: any;

  phaserGame: Phaser.Game;
  gameConfig: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    height: 600,
    width: 800,
    // scene: [ this.mainScene ],
    parent: 'canvas-wrapper',
    backgroundColor: '0x111111',
    physics: {
      default: 'arcade',
      arcade: {
        gravity: { y: 0 }
      }
    }
  };

  constructor(
    private mainScene: MainScene,
    private ngZone: NgZone,
    private wsService: WsService,
  ) {
    this.wsService.socketSubject.subscribe(
      msg => {
        console.log('message received: ' + msg); // Called whenever there is a message from the server.
        if (msg.type === MessageTypesIn.VOTE_START) {
          this.voteInProgress = true;
          this.voteResult = '';
        }

        if (msg.type === MessageTypesIn.VOTES){
          this.voteResult = (<any>msg.data).votes;
          this.voteInProgress = false;
          this.voted = false;
        }
        // console.log(msg.type + '===' + MessageTypesIn.ALL_ROLES);
        // console.log(msg.type === MessageTypesIn.ALL_ROLES);
        // if (msg.type === MessageTypesIn.ALL_ROLES){
        //   this.allRoles = msg.data;
        //   console.log(this.allRoles);
        //   console.log(this.role=='policajt');
        // }
      },
      err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      () => console.log('complete') // Called when connection is closed (for whatever reason).
    );
  }

  ngOnInit() {
    this.role = window.history.state.role;
    this.allRoles = window.history.state.allRoles;
    // this.startGame(window.history.state);
    // this.wsService.socketSubject
    //   .pipe( filter(msg => msg.type === MessageTypesIn.START_GAME) )
    //   .subscribe({
    //     next: msg => {
    //       this.gameStarted = true;
    //       this.startGame(msg.data)
    //     }
    //   });
  }

  startVote(){
    this.wsService.sendMessage({
      type: MessageTypesOut.VOTE_START,
      data: { },
    });
  }

  vote(value:string){
    this.voted = true;
    this.wsService.sendMessage({
      type: MessageTypesOut.PRIVATE_VOTE,
      data: { vote: value },
    });
  }

  startGame(data: any) {
    this.ngZone.runOutsideAngular(() => {
      this.phaserGame = new Phaser.Game(this.gameConfig);
      this.phaserGame.scene.add('main', this.mainScene, true, data);
    });
  }
}
