import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../game/game-data.service';
import { GameType } from '../types/game-type.enum';
import { WsService } from '../ws.service';
import { MessageTypesOut } from '../types/message-types-out.enum';
import { Router } from '@angular/router';
import {MessageTypesIn} from "../types/message-types-in.enum";

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.less']
})
export class NewGameComponent implements OnInit {
  duel: GameType = GameType.DUEL;
  classic: GameType = GameType.CLASSIC;

  joinableGames = [];

  constructor(
    private gameDataService: GameDataService,
    private wsService: WsService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.wsService.socketSubject.subscribe(
      msg => {
        console.log('message received NG: ' + msg); // Called whenever there is a message from the server.
        if (msg.type === MessageTypesIn.JOINABLE_GAME){
          this.joinableGames.push(<any>msg.data);
        }

        // if (msg.type === MessageTypesIn.UNFINISHED_GAMES){
        //   (<Map<number, any>>msg.data).forEach(((value, key) => {
        //     value.forEach((game)=>{
        //       this.joinableGames.push({
        //         playerCount: key,
        //         playerName: game.players[0].nickname,
        //         playerEmail: game.players[0].email,
        //         playerId: game.players[0].id,
        //       });
        //     })
        //   }));
        // }
      },
      err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      () => console.log('complete') // Called when connection is closed (for whatever reason).
    );
  }

  startGame(playerCount:number){
    this.wsService.sendMessage({
      type: MessageTypesOut.START_GAME,
      data: {playerCount},
    });
  }

  join(playerId){
    this.wsService.sendMessage({
      type: MessageTypesOut.JOIN_GAME,
      data: {playerId},
    });
  }

  joinGame(gameType: GameType) {
    this.wsService.sendMessage({
      type: MessageTypesOut.JOIN_GAME,
      data: { gameType },
    });
    // this.router.navigate(['/game']);
    // this.gameDataService.joinGame(gameType).subscribe({
    //   next: () => null,
    // });
  }

}
